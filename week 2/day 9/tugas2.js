// Create an array 
let orang = [
    {
        name : "Laurent",
        status : "Negative",
    },
    {
        name : "Vie",
        status : "Positive",
    },
    {
        name : "Venus",
        status : "Suspect",
    },
    {
        name : "Langit",
        status : "Suspect",
    },
    {
        name : "el",
        status : "Negative",
    },
    {
        name : "vano",
        status : "Positive",
    },
    {
        name : "levi",
        status : "Negative",
    },
    {
        name : "kitaro",
        status : "Suspect",
    },
    {
        name : "vay",
        status : "Positive",
    },
];

function input (){



let kasus = 1;
// Statement for choice to see status of humans
switch (kasus){
    case 1:
        let positivePeople = orang.filter(person => person.status === 'Positive');
        console.log(positivePeople);
    break;

    case 2:
        let negativePeople = orang.filter(person => person.status === 'Negative');
        console.log(negativePeople);
    break;

    case 3:
        let suspectPeople = orang.filter(person => person.status === 'Suspect');
        console.log(suspectPeople);
    break;

    default:
        console.log('End');
}
}

module.exports = input;