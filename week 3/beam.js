const ThreeDimention = require (`./threedimention`);

class Beam extends ThreeDimention {
    constructor(width, length, height) {
        super ('Beam');
        this.width = width;
        this.length = length;
        this.height = height;
    }

    calculateArea(){
        super.calculateArea();
        return this.width * this.length * this.height;
    }
    calculateCircumference () {
        return (this.width + this.length + this.height) * 3;
    }
}

module.exports = Beam;