const axios = require ('axios');


let urlCanvas = 'https://jsonplaceholder.typicode.com/posts/1/comments';
let urlBrush = 'https://fakestoreapi.com/products?limit=5';
let urlWaterColor= 'https://fakestoreapi.com/products/category/jewelery';
let data = {};


axios
    .get(urlCanvas)
    .then((response) => {
        data = { Canvas: response.data};
        // console.log(data.Canvas);
        
        return axios.get(urlBrush);
    })

    .then ((response) => {
        data = { ...data, Brush:response.data};

        return axios.get(urlWaterColor);
    })

    .then((response) => {
        data = { ...data, WaterColor: response.data };
        console.log(data);
    })
    .catch((err) => {
        console.log(err.massage);
    });