const fetch = require ("node-fetch");

let urlCanvas = 'https://jsonplaceholder.typicode.com/posts/1/comments';
let urlBrush = 'https://fakestoreapi.com/products?limit=5';
let urlWaterColor= 'https://fakestoreapi.com/products/category/jewelery';
let data = {};

const asyncFetch = async () => {
    try {
        let responseCanvas = await fetch(urlCanvas);
        let responseBrush = await fetch(urlBrush);
        let responseWaterColor = await fetch(urlWaterColor);

        let response = await Promise.all([
            responseCanvas.json(),
            responseBrush.json(),
            responseWaterColor.json(),
        ]);

        data = {
            canvas: response[0],
            brush: response[1],
            watercolor: response[2],
        };

        console.log(data);
    } catch (error) {
        console.error(error.message);
    }
};

asyncFetch();