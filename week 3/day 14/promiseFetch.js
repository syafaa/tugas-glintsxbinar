const fetch = require('node-fetch')

let urlCanvas = 'https://jsonplaceholder.typicode.com/posts/1/comments';
let urlBrush = 'https://fakestoreapi.com/products?limit=5';
let urlWaterColor= 'https://fakestoreapi.com/products/category/jewelery';
let data = {};

fetch (urlCanvas)
.then ((canvas) => canvas.json())
.then ((canvasJson) => {
    data = {canvas: canvasJson}
    return fetch(urlBrush)
})
.then ((urlBrush) => urlBrush.json()) 
.then((brushJson) => {
    data = {...data, brush: brushJson}
    return fetch(urlWaterColor)
})
.then ((urlWaterColor) => urlWaterColor.json()) 
.then((waterJson) => {
    data = {...data, color: waterJson}
    console.log(data)

})

.catch ((err) => {
    console.log(err.message)
})