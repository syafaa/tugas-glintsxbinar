const axios = require('axios');

let urlCanvas = 'https://jsonplaceholder.typicode.com/posts/1/comments';
let urlBrush = 'https://fakestoreapi.com/products?limit=5';
let urlWaterColor= 'https://fakestoreapi.com/products/category/jewelery';
let data = {};

const fetchApi = async () => {

  try {
    const response = await Promise.all([
      axios.get(urlCanvas),
      axios.get(urlBrush),
      axios.get(urlWaterColor),
    ]);


    data = {
      canvas:response[0].data,
      brush:response [1].data,
      watercolor:response[2].data,
    }
  
   

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
}

fetchApi();