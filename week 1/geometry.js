// 
// rumus balok
function balokReturn(lebar, panjang, tinggi) {
    console.log('return:' + lebar * panjang * tinggi);
    return lebar * panjang * tinggi;
}

let balokOne = balokReturn(10, 20, 30);
let balokTwo = balokReturn(5, 10, 15);
console.log('balok 1 + balok 2:' + [balokOne + balokTwo]);

// rumus volume Prisma

function prismaReturn(luasAlas, tinggi) { 
    console.log('return:' + luasAlas * tinggi);
    return luasAlas * tinggi;
}

let prismaSatu = prismaReturn(200, 8);
let prismaDua = prismaReturn(100, 80);
console.log('prismaSatu + prismaDua:' + [prismaSatu + prismaDua]);

// rumus volume prisma argumen
function prismaArgumen (luasAlas, tinggi) {
    console.log(luasAlas * tinggi);
}
prismaArgumen(200, 8)