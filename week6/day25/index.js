// Import express
const express = require("express");

// Import routes
const person = require("./routes/user");
const assignment = require("./routes/task");

// Import error Handler
const errorHandler = require("./middlewares/errorHandler");

// Define port
const port = process.env.PORT || 3000;

// Make express app
const app = express();

// Enable req.body
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Make routes
app.use("/person", person);
app.use("/assignment", assignment);

// If routes not exist
app.all("*", (req, res, next) => {
  next({ statusCode: 404, message: "Endpoint not found" });
});

// Enable error handler
app.use(errorHandler);

// Run server
app.listen(port, () => {
  console.log(`Server running on ${port}`);
});
